import Vue from 'vue'
import Router from 'vue-router'
import GameDisplay from '@/components/GameDisplay'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'GameDisplay',
      component: GameDisplay
    }
  ]
})
