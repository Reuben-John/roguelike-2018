import firebase from 'firebase'
import firestore from 'firebase/firestore'

// Initialize Firebase
var config = {
  apiKey: 'AIzaSyAt_gj3b9KG2Ko1imPVEZjA4XrfiKsYktQ',
  authDomain: 'rotjs-roguelike.firebaseapp.com',
  databaseURL: 'https://rotjs-roguelike.firebaseio.com',
  projectId: 'rotjs-roguelike',
  storageBucket: '',
  messagingSenderId: '492214146372'
}
const firebaseApp = firebase.initializeApp(config)
firebaseApp.firestore().settings({ timestampsInSnapshots: true })

// export firestore database
export default firebaseApp.firestore()
