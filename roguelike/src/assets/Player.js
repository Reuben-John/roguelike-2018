import ROT from 'rot-js'
import { Game } from '@/assets/game'

export let Player = {
  x: null,
  y: null,

  draw () {
    Game.display.draw(this.x, this.y, '@', '#ff0')
  },

  act () {
    Game.engine.lock()
    // wait for user input; do stuff when user hits a key
    window.addEventListener('keydown', this)
  },

  getX () {
    return this.x
  },

  getY () {
    return this.y
  },

  handleEvent (e) {
    let keyMap = {
      38: 0,
      33: 1,
      39: 2,
      34: 3,
      40: 4,
      35: 5,
      37: 6,
      36: 7
    }
    let code = e.keyCode
    // one of numpad directions?
    if (!(code in keyMap)) {
      return
    }

    // is there a free space?
    let dir = ROT.DIRS[8][keyMap[code]]
    let newX = this.x + dir[0]
    let newY = this.y + dir[1]

    let newKey = newX + ',' + newY
    if (!(newKey in Game.map)) {
      // Cannot move in this direction
      return
    }

    console.log('moved')
    Game.display.draw(this.x, this.y, Game.map[this.x + ',' + this.y])
    this.x = newX
    this.y = newY
    this.draw()
    window.removeEventListener('keydown', this)
    Game.engine.unlock()
  }
}
