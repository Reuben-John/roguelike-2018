import ROT from 'rot-js'
import { Player } from '@/assets/player.js'

export let Game = {
  display: null,
  player: null,
  mapWidth: 80,
  mapHeight: 50,
  map: {},
  engine: null,

  init () {
    this.display = new ROT.Display()
    this.display.setOptions({
      width: this.mapWidth,
      height: this.mapHeight,
      forceSquareRatio: true,
      spacing: 1.1
    })

    document.body.appendChild(this.display.getContainer())

    this.generateMap()
    this.player.draw()

    let scheduler = new ROT.Scheduler.Simple()
    scheduler.add(this.player, true)

    this.engine = new ROT.Engine(scheduler)
    this.engine.start()
  },

  generateMap () {
    // TODO Generate random map

    let map = new ROT.Map.Digger(this.mapWidth, this.mapHeight, {
      roomWidth: [4, 12],
      roomHeight: [4, 9],
      corridorLength: [3, 8],
      dugPercentage: 0.25
    })
    let freeCells = []

    let digCallback = function (x, y, value) {
      if (value) {
        return
      } /* do not store walls */

      let key = x + ',' + y
      this.map[key] = '.'
      freeCells.push(key)
    }
    map.create(digCallback.bind(this))

    this.drawWholeMap()
    this.createPlayer(freeCells)
  },

  drawWholeMap () {
    for (let key in this.map) {
      let parts = key.split(',')
      let x = parseInt(parts[0])
      let y = parseInt(parts[1])
      this.display.draw(x, y, this.map[key])
    }
  },

  createPlayer (freeCells) {
    let index = Math.floor(ROT.RNG.getUniform() * freeCells.length)
    let key = freeCells.splice(index, 1)[0]
    let parts = key.split(',')
    let x = parseInt(parts[0])
    let y = parseInt(parts[1])
    // this.player = new Player(x, y)
    this.player = Object.create(Player)
    this.player.x = x
    this.player.y = y
  }
}
